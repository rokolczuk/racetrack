﻿using UnityEngine;
using UnityEditor;
using Game.Model.Path;
using Game.View;

[CustomEditor(typeof(PathComponent))]
public class PathComponentInspector : Editor 
{
	private SerializedProperty pathPoints;

	private SerializedObject pathData;

	private PathComponent component;

	private void OnEnable()
	{
		component = (PathComponent)serializedObject.targetObject;

		if(serializedObject.FindProperty("pathData").objectReferenceValue != null)
		{
			pathData = new SerializedObject(serializedObject.FindProperty("pathData").objectReferenceValue);
			pathPoints = pathData.FindProperty("Points");
		}
	}

	public void OnSceneGUI()
	{
		for(int i = 0; i < pathPoints.arraySize; i++)
		{
			SerializedProperty pathPoint = pathPoints.GetArrayElementAtIndex(i);
			SerializedProperty point = pathPoint.FindPropertyRelative("Point");

			point.vector3Value = Handles.DoPositionHandle(point.vector3Value, Quaternion.identity);

			if(Handles.Button(point.vector3Value + Vector3.up*2, Quaternion.identity, 1f, 1f, Handles.SphereCap))
			{
				pathPoints.DeleteArrayElementAtIndex(i);
			}
		}

		pathData.ApplyModifiedProperties();
		serializedObject.ApplyModifiedProperties();
	}
		

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		if(pathPoints != null)
		{
			for(int i = 0; i < pathPoints.arraySize; i++)
			{
				SerializedProperty pathPoint = pathPoints.GetArrayElementAtIndex(i);
				SerializedProperty point = pathPoint.FindPropertyRelative("Point");

				GUILayout.BeginHorizontal();

				EditorGUILayout.PropertyField(point, new GUIContent("Point " + (i+1)));

				GUI.color = Color.red;

				if(GUILayout.Button("Delete"))
				{
					pathPoints.DeleteArrayElementAtIndex(i);
					i--;
				}
				GUI.color = Color.green;

				if(GUILayout.Button("Add"))
				{
					pathPoints.InsertArrayElementAtIndex(i+1);
				}

				GUI.color = Color.white;

				if(GUILayout.Button("Select anchor"))
				{
					Selection.activeGameObject = component.transform.FindChild("AnchorPoint" + i).gameObject;
				}

				GUILayout.EndHorizontal();
			
			}
							
			pathData.ApplyModifiedProperties();
			serializedObject.ApplyModifiedProperties();
		}
			
	}


}

