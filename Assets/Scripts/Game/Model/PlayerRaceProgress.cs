﻿using System;
using UnityEngine;

namespace Game.Model
{
	public class PlayerRaceProgress: IComparable<PlayerRaceProgress>
	{
		public float DistanceTraveled { get; private set; }
		public int NumLapsTraveled { get; private set; }
		public Player Player { get; private set; }

		private float totalDistance;

		private float raceProgress = 0f;

		public PlayerRaceProgress(Player player, int numLapsToFinish, float lapDistance)
		{
			this.Player = player;


			totalDistance = numLapsToFinish * lapDistance;
		}

		public void AppendDistanceTraveled(float distanceTraveled)
		{
			this.DistanceTraveled += distanceTraveled;
			raceProgress = Mathf.Clamp01(DistanceTraveled / totalDistance);
		}

		public void SetLapDone()
		{
			NumLapsTraveled++;
		}

		public float GetRaceProgress()
		{
			return raceProgress;
		}

		#region IComparable implementation

		public int CompareTo (PlayerRaceProgress obj)
		{
			if(GetRaceProgress() == obj.GetRaceProgress())
			{
				return 0;
			}

			return GetRaceProgress() > obj.GetRaceProgress() ? -1 : 1;
		}

		#endregion
	}
}