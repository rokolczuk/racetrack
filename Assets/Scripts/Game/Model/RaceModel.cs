﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using Game.View;
using Game.Model;
using Game.Model.Path;
using UnityEngine.Assertions;

public class RaceModel 
{
	private List<Car> cars = new List<Car>();
	private ReadOnlyCollection<Car> readOnlyCars;

	private List<Player> players = new List<Player>();
	private ReadOnlyCollection<Player> readOnlyPlayers;

	private List<IObstacle> obstacles = new List<IObstacle>();
	private ReadOnlyCollection<IObstacle> readOnlyObstacles;

	private Dictionary<Player, PlayerRaceProgress> raceProgress = new Dictionary<Player, PlayerRaceProgress>();
	private List<PlayerRaceProgress> sortedRaceProgress = new List<PlayerRaceProgress>();

	private float lapDistance = 0f;
	public int NumLaps;

	public bool RaceOver = false;

	public  ReadOnlyCollection<Car> Cars
	{
		get 
		{
			return readOnlyCars;
		}
	}

	public  ReadOnlyCollection<Player> Players
	{
		get 
		{
			return readOnlyPlayers;
		}
	}

	public  ReadOnlyCollection<IObstacle> Obstacles
	{
		get 
		{
			return readOnlyObstacles;
		}
	}

	public void SetLapDistance (float lapDistance)
	{
		this.lapDistance = lapDistance;
	}

	public void RegisterCar(Car car)
	{
		Assert.IsTrue(NumLaps > 0, "Set number of laps first");
		Assert.IsTrue(lapDistance > 0.0f, "Set lap distance first");

		Register<Car>(car, cars, ref readOnlyCars);
		Register<IObstacle>(car, obstacles, ref readOnlyObstacles);

		PlayerRaceProgress playerRaceProgress = new PlayerRaceProgress(car.Player, NumLaps, lapDistance);
		raceProgress.Add(car.Player, playerRaceProgress);
		sortedRaceProgress.Add(playerRaceProgress);
	}

	public void RegisterPlayer(Player player)
	{
		Register<Player>(player, players, ref readOnlyPlayers);
	}

	public PlayerRaceProgress GetPlayerRaceProgress(Player player)
	{
		return raceProgress[player];
	}

	public List<PlayerRaceProgress> GetSortedRaceProgress()
	{
		sortedRaceProgress.Sort(
			(PlayerRaceProgress x, PlayerRaceProgress y) => x.CompareTo(y) 
		);

		return sortedRaceProgress;
	}

	public void RegisterObstacle(Obstacle obstacle)
	{
		Register<IObstacle>(obstacle, obstacles, ref readOnlyObstacles);
	}

	private void Register<T>(T instance, List<T> collection, ref ReadOnlyCollection<T> readOnlyCollection)
	{
		collection.Add(instance);
		readOnlyCollection = collection.AsReadOnly();
	}
}
