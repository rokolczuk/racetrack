﻿using UnityEngine;

using System.Collections.Generic;
namespace Game.Model.Path
{
	[CreateAssetMenu (menuName = "Racetrack/New Path", fileName = "Path.asset")]
	public class PathData : ScriptableObject, ISerializationCallbackReceiver
	{
		[SerializeField]
		public PathPoint[] Points;

		public float LapDistance { get; private set; }

		public PathPoint StartNode { get; private set; }
		#region ISerializationCallbackReceiver implementation

		public void OnBeforeSerialize ()
		{
		}

		public void OnAfterDeserialize ()
		{
			LapDistance = 0;
			StartNode = Points[0];

			for(int i = 0; i < Points.Length; i++)
			{
				Points[i].Previous = Points[GetPreviousPointIndex(i)];
				Points[i].Next = Points[GetNextPointIndex(i)];

				Points[i].DistanceToPrevious = Vector3.Distance(Points[i].Point, Points[i].Previous.Point);
				Points[i].DistanceToNext = Vector3.Distance(Points[i].Point, Points[i].Next.Point);

				LapDistance += Points[i].DistanceToNext;
			}
		}

		#endregion

		private int GetPreviousPointIndex(int index)
		{
			if(index == 0)
			{
				return Points.Length - 1;
			}

			return index - 1;
		}

		private int GetNextPointIndex(int index)
		{
			if(index == Points.Length - 1)
			{
				return 0;
			}

			return index + 1;
		}
	}
}
