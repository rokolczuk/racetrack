﻿using System;
using UnityEngine;

namespace Game.Model.Path
{
	[Serializable]
	public class PathPoint 
	{
		public Vector3 Point;

		[NonSerialized]
		public float DistanceToPrevious;
		[NonSerialized]
		public float DistanceToNext;

		[NonSerialized]
		public PathPoint Previous;
		[NonSerialized]
		public PathPoint Next;
	}
}
