﻿using System;
using UnityEngine;
using System.Globalization;
using Shared.Util;

namespace Game.Model
{
	[Serializable]
	public class GameConfiguration
	{
		[SerializeField]
		private int numPlayers;

		public int NumPlayers { get { return numPlayers; } }

		[SerializeField]
		private int lapsNumber;

		public int LapsNumber { get { return lapsNumber; } }

		[SerializeField]
		private int playersInstantiationDelay;

		public int PlayersInstantiationDelay { get { return playersInstantiationDelay; } }
	}

	[Serializable]
	public class Player
	{
		public string Name;
		public int Velocity;
		public string Icon;

		[SerializeField]
		private string Color;

		public Color GetColor ()
		{
			return ColorUtil.HexToColor(Color);
		}
	}

	[Serializable]
	public class Config
	{
		public GameConfiguration GameConfiguration;
		public Player[] Players;
	}
}

