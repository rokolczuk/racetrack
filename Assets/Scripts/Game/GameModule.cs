﻿using System;
using UnityEngine;
using Shared;
using Shared.DependencyInjection;
using Shared.Events;
using System.IO;
using Shared.Loading;
using Game.Model;
using Game.Loading;

namespace Game
{
	public class GameModule : IModule
	{
		private InjectProvider injectProvider;

		private LoadingQueue loadingQueue;

		#region IModule implementation

		public GameModule()
		{
		}

		public void Initialise ()
		{
			InjectProvider.CreateProvider (Context.Game);
			injectProvider = InjectProvider.GetProvider (Context.Game);

			injectProvider.Map<EventDispatcher>();
			injectProvider.Map<RaceModel>();

			CreateLoadingQueue();

			//speed up simulation so it doesn't look dull
			Time.timeScale = 3.0f;

		}
			
		public void Dispose ()
		{
			loadingQueue.Unload();
			InjectProvider.DestroyProvider(Context.Game);

			loadingQueue = null;

			Time.timeScale = 1.0f;
		}

		public string GetSceneName ()
		{
			return "game";
		}

		public LoadingQueue LoadingQueue
		{
			get
			{
				return loadingQueue;
			}
		}

		#endregion

		private void CreateLoadingQueue ()
		{
			loadingQueue = new LoadingQueue();
			loadingQueue.AddTask(new JsonLoadTask<Config>(Application.streamingAssetsPath + "/Data.txt", injectProvider));
			loadingQueue.AddTask(new LoadRaceTask());
		}
	}
}