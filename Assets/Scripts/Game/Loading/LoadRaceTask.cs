﻿using Shared.Loading;
using Shared.DependencyInjection;
using Game.Model;
using Shared;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Loading
{
	public class LoadRaceTask: LoadingQueue
	{
		[Inject(Context.Game)]
		private Config config;

		[Inject(Context.Game)]
		private RaceModel raceModel;

		private List<Player> players;

		public override void Load ()
		{
			this.Inject();
			SelectPlayers();
			RegsiterPlayersAndLoadTheirIcons();
			base.Load ();
		}


		private void SelectPlayers ()
		{
			players = new List<Player>(config.Players);

			while(players.Count > config.GameConfiguration.NumPlayers)
			{
				players.RemoveAt(UnityEngine.Random.Range(0, players.Count));
			}

			players.Sort((x, y) => x.Velocity - y.Velocity);
		}

		private void RegsiterPlayersAndLoadTheirIcons ()
		{
			for(int i = 0; i < players.Count; i++)
			{
				AddTask(new LoadImageTask(players[i].Icon));
				raceModel.RegisterPlayer(players[i]);
			}
		}
	}
}

