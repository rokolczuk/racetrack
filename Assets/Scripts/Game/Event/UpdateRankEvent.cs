﻿using System;
using System.Collections.Generic;
using Game.Model;

namespace Game.Event
{
	public class UpdateRankEvent
	{
		public readonly List<PlayerRaceProgress> sortedPlayerRaceProgress;

		public UpdateRankEvent (List<PlayerRaceProgress> sortedPlayerRaceProgress)
		{
			this.sortedPlayerRaceProgress = sortedPlayerRaceProgress;
		}
	}
}

