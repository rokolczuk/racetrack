﻿using System;
using Game.Model;

namespace Game.Event
{
	public class RaceOverEvent
	{
		public readonly Player Winner;

		public RaceOverEvent (Player winner)
		{
			this.Winner = winner;
		}
	}
}

