﻿using UnityEngine;
using Game.Model;
using Shared;
using Shared.DependencyInjection;
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using Shared.Events;

namespace Game.View
{
	public class RaceController : MonoBehaviour 
	{

		[Inject(Context.Game)]
		private Config config;

		[Inject(Context.Game)]
		private RaceModel raceModel;

		[Inject(Context.Game)]
		private EventDispatcher eventDispatcher;

		[SerializeField]
		private PathComponent path;

		[SerializeField]
		private GameObject carPrefab;

		[SerializeField]
		private Obstacle[] obstacles;

		private void Awake()
		{
			this.Inject();

			RegisterObstacles();

			StartRace();
		}

		private void RegisterObstacles ()
		{
			for(int i = 0; i < obstacles.Length; i++)
			{
				raceModel.RegisterObstacle(obstacles[i]);
			}
		}

		private void StartRace ()
		{
			raceModel.SetLapDistance(path.PathData.LapDistance);
			raceModel.NumLaps = config.GameConfiguration.LapsNumber;
			StartCoroutine(SpawnPlayers(raceModel.Players, config.GameConfiguration.PlayersInstantiationDelay));
		}
			
		private IEnumerator SpawnPlayers (ReadOnlyCollection<Player> players, int playersInstantiationDelay)
		{
			int playerIndex = 0;

			while(playerIndex < players.Count)
			{
				GameObject carGameObject = GameObject.Instantiate(carPrefab);

				Car car = carGameObject.GetComponent<Car>();
				car.transform.position = path.StartPosition;
				car.Player = players[playerIndex];
					
				CarController carController = carGameObject.GetComponent<CarController>();

				raceModel.RegisterCar(car);
				carController.SetPath(path.PathData);

				playerIndex++;

				yield return new WaitForSeconds(config.GameConfiguration.PlayersInstantiationDelay / 1000);
			}
		}
	}
}