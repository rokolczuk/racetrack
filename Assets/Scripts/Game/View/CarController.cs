﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Model.Path;
using Shared.DependencyInjection;
using Shared;
using Shared.Events;
using Game.Event;
using Game.Model;

namespace Game.View
{
	[RequireComponent(typeof(Car))]
	public class CarController : MonoBehaviour
	{
		private PathPoint currentWaypoint;
		private PathPoint nextWaypoint;

		private Car car;

		[SerializeField]
		private float maxSteerAngle;

		[SerializeField]
		private float brakeThreshold;

		[SerializeField]
		private float dangerIndicatorToBrakePowerMultiplier;

		[SerializeField]
		private float avoidVectorMultiplier;

		[SerializeField]
		private int iterationsToUpdatePath = 10;

		private int numIterationsPassed = 0;

		[Inject(Context.Game)]
		private RaceModel raceModel;

		[Inject(Context.Game)]
		private EventDispatcher eventDispatcher;

		private PathPoint startWaypoint;

		private float avoidVectorMagnitude;

		private float steer;
		private float brake;
		private float acceleration;

		private void Awake()
		{
			car = GetComponent<Car>();
			this.Inject();
		}

		public void SetPath (PathData path)
		{
			startWaypoint = path.StartNode;
			currentWaypoint = path.StartNode;
			nextWaypoint = currentWaypoint.Next;
			NextWaypoint();
		}

		private void NextWaypoint ()
		{
			PlayerRaceProgress progress = raceModel.GetPlayerRaceProgress(car.Player);
			progress.AppendDistanceTraveled(currentWaypoint.DistanceToPrevious);
			eventDispatcher.Dispatch<UpdateRankEvent>(new UpdateRankEvent(raceModel.GetSortedRaceProgress()));

			if(currentWaypoint.Next == startWaypoint)
			{
				progress.SetLapDone();

				if(progress.NumLapsTraveled == raceModel.NumLaps && !raceModel.RaceOver)
				{
					raceModel.RaceOver = true;
					eventDispatcher.Dispatch<RaceOverEvent>(new RaceOverEvent(progress.Player));
				}
			}

			nextWaypoint = nextWaypoint.Next;
			currentWaypoint = currentWaypoint.Next;
		}

		private void FixedUpdate()
		{
			if(++numIterationsPassed >= iterationsToUpdatePath)
			{
				numIterationsPassed = 0;

				float distanceToNextWaypoint = Vector3.Distance(transform.position, nextWaypoint.Point);

				if (distanceToNextWaypoint < currentWaypoint.DistanceToNext)
				{
					NextWaypoint();
				}

				Vector3 directionVector = GetDirectionVector();

				Debug.DrawLine(transform.position, transform.TransformPoint(directionVector*12f), Color.white);

				steer = maxSteerAngle * (directionVector.x / directionVector.magnitude);
				acceleration = 1.0f - steer / maxSteerAngle; //slow down on turns
				float dangerIndicator = Mathf.Abs(steer) * car.Speed.magnitude;
				brake = 0f;

				if(dangerIndicator >= brakeThreshold)
				{
					brake = (dangerIndicator - brakeThreshold) * dangerIndicatorToBrakePowerMultiplier;
				}
			}

			car.Turn(steer);
			car.Accelerate(acceleration);
			car.Brake(brake);


//			Debug.Log("directionVector.x: " + directionVector.x);
//			Debug.Log("accelerate: " + acceleration);
//			Debug.Log("current speed: " + car.Speed);
//			Debug.Log("steer: " + steer);
//			Debug.Log("dangerIndicator: " + dangerIndicator);
//			Debug.Log("brake: " + brake);
		}

		private Vector3 GetDirectionVector ()
		{
			Vector3 followPathDirectionVector = GetFollowPathDirectionVector();
			Debug.DrawLine(transform.position, transform.TransformPoint(followPathDirectionVector), Color.cyan);

			Vector3 avoidOtherCarsVector = GetAvoidOtherCarsVector();
			Debug.DrawLine(transform.position, transform.TransformPoint(avoidOtherCarsVector), Color.blue);

			return followPathDirectionVector.normalized + avoidOtherCarsVector.normalized * avoidVectorMagnitude;
		}

		//this method is lerping between current and next waypoint based on how close the car is to current waypoint
		private Vector3 GetFollowPathDirectionVector ()
		{
			float distanceToCurrentWaypoint = Vector3.Distance(transform.position, currentWaypoint.Point);

			Vector3 localWaypointPosition = transform.InverseTransformPoint(new Vector3(currentWaypoint.Point.x, transform.position.y, currentWaypoint.Point.z));
			Vector3 localNextWaypointPosition = transform.InverseTransformPoint(new Vector3(nextWaypoint.Point.x, transform.position.y, nextWaypoint.Point.z));

			localWaypointPosition.y = localNextWaypointPosition.y = 0f;

			float lerp = 1.0f - Mathf.Clamp01(distanceToCurrentWaypoint / currentWaypoint.DistanceToPrevious);

			Vector3 localDistanceInBetweenCurrentAndNextWaypoint = Vector3.Lerp(localWaypointPosition, localNextWaypointPosition, lerp);

			return localDistanceInBetweenCurrentAndNextWaypoint;
		}

		private Vector3 GetAvoidOtherCarsVector ()
		{
			int numCarsInDangerZone = 0;
			Vector3 avoidObstaclesPosition = Vector3.zero;
			float closestObstacleDistance = 1000f;
			avoidVectorMagnitude = 0f;

			for (int i = 0; i < raceModel.Obstacles.Count; i++)
			{
				float distanceToObstacle = Vector3.Distance (transform.position, raceModel.Obstacles[i].transform.position);

				if (raceModel.Obstacles[i] != (car as IObstacle) && distanceToObstacle < raceModel.Obstacles[i].MinAvoidDistance)
				{
					numCarsInDangerZone++;
					closestObstacleDistance = Mathf.Min (distanceToObstacle, closestObstacleDistance);
					avoidObstaclesPosition -= transform.InverseTransformPoint(raceModel.Obstacles[i].transform.position);
					avoidVectorMagnitude = Mathf.Max(avoidVectorMagnitude, 1f - Mathf.Clamp01(closestObstacleDistance / raceModel.Obstacles[i].MinAvoidDistance));
				}
			}

			if(numCarsInDangerZone == 0)
			{
				return avoidObstaclesPosition;
			}
				

			avoidObstaclesPosition /= numCarsInDangerZone;
			avoidObstaclesPosition.y = 0;

			return avoidObstaclesPosition;
		}
	}
}
