﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Model;
using Shared.DependencyInjection;
using Shared;
using Shared.Events;
using Game.Event;
using Game.Evemt;

public class RaceOverPopupController : MonoBehaviour 
{
	[SerializeField]
	private DynamicImage playerIcon;

	[SerializeField]
	private Text playerName;

	[SerializeField]
	private Button restartButton;

	[SerializeField]
	private Button closeButton;

	[Inject(Context.Game)]
	private EventDispatcher eventDispatcher;

	private void Awake()
	{
		this.Inject();
		restartButton.onClick.AddListener(OnRestartButtonClicked);
		closeButton.onClick.AddListener(OnCloseButtonClicked);
		eventDispatcher.AddEventListener<RaceOverEvent>(OnRaceOver);
		gameObject.SetActive(false);
	}

	private void OnRaceOver (RaceOverEvent e)
	{
		gameObject.SetActive(true);
		SetPlayer(e.Winner);
	}

	private void OnDestroy()
	{
		restartButton.onClick.RemoveListener(OnRestartButtonClicked);
		eventDispatcher.RemoveEventListener<RaceOverEvent>(OnRaceOver);
	}

	private void OnRestartButtonClicked ()
	{
		eventDispatcher.Dispatch<RestartGameEvent>(new RestartGameEvent());
		gameObject.SetActive(false);

	}

	void OnCloseButtonClicked ()
	{
		eventDispatcher.Dispatch<LaunchMenuEvent>(new LaunchMenuEvent());
		gameObject.SetActive(false);
	}

	public void SetPlayer(Player player)
	{
		playerIcon.LoadImage(player.Icon);
		playerName.text = player.Name;
	}
}
