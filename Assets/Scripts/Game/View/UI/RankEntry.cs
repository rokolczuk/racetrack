﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Model;

public class RankEntry : MonoBehaviour 
{
	[SerializeField]
	private ProgressBar progressBar;

	[SerializeField]
	private Text driverName;

	[SerializeField]
	private DynamicImage playerIcon;

	private void Awake()
	{
		progressBar.Progress = 0;
	}

	public void SetPlayerProgress (PlayerRaceProgress raceProgress)
	{
		driverName.text = raceProgress.Player.Name;
		playerIcon.LoadImage(raceProgress.Player.Icon);
		progressBar.Progress = raceProgress.GetRaceProgress();
	}
}
