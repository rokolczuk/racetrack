﻿using System;
using UnityEngine;
using Shared.DependencyInjection;
using Shared;
using System.Collections.Generic;
using Shared.Events;
using Game.Event;
using Game.Model;

namespace Game.View.Hud
{
	
	public class Rank: MonoBehaviour
	{
		[SerializeField]
		private int numDriversDisplayed;

		[SerializeField]
		private GameObject driverRankPrefab;

		[Inject(Context.Game)]
		private RaceModel raceModel;

		[Inject(Context.Game)]
		private EventDispatcher eventDispatcher;

		private List<RankEntry> entries = new List<RankEntry>();

		private void Awake()
		{
			this.Inject();
			eventDispatcher.AddEventListener<UpdateRankEvent>(OnUpdateRankEvent);
			eventDispatcher.AddEventListener<RaceOverEvent>(OnRaceOver);
			CreateEntries();
		}

		private void CreateEntries ()
		{
			List<PlayerRaceProgress> playersRaceProgress = raceModel.GetSortedRaceProgress();

			while(entries.Count < numDriversDisplayed)
			{
				GameObject newEntry = GameObject.Instantiate(driverRankPrefab) as GameObject;
				newEntry.transform.SetParent(transform);
				RankEntry rankEntry = newEntry.GetComponent<RankEntry>();
				entries.Add(rankEntry);

				if(playersRaceProgress.Count > entries.Count - 1)
				{
					rankEntry.SetPlayerProgress(playersRaceProgress[entries.Count-1]);
				}
			}
		}

		private void OnDestroy()
		{
			eventDispatcher.RemoveEventListener<UpdateRankEvent>(OnUpdateRankEvent);
			eventDispatcher.RemoveEventListener<RaceOverEvent>(OnRaceOver);
		}

		private void OnUpdateRankEvent (UpdateRankEvent updateRankEvent)
		{
			int numEntriesToFill = Math.Min(updateRankEvent.sortedPlayerRaceProgress.Count, numDriversDisplayed);

			for(int i = 0; i < numEntriesToFill; i++)
			{
				entries[i].SetPlayerProgress(updateRankEvent.sortedPlayerRaceProgress[i]);
			}
		}

		private void OnRaceOver (RaceOverEvent e)
		{
			eventDispatcher.RemoveEventListener<UpdateRankEvent>(OnUpdateRankEvent);
		}
	}
}