﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Model.Path;

namespace Game.View
{
	[ExecuteInEditMode]
	public class PathComponent : MonoBehaviour
	{
		[SerializeField]
		private PathData pathData;

		public Vector3 StartPosition
		{
			get { return pathData.Points[0].Point; }
		}

		public PathData PathData
		{
			get { return pathData; }
		}
			
		private void OnDrawGizmos ()
		{
			if (pathData != null)
			{
				Gizmos.color = Color.green;
				Gizmos.DrawSphere (pathData.StartNode.Point, 1f);

				Gizmos.color = Color.yellow;
				Gizmos.DrawLine (pathData.StartNode.Point, pathData.StartNode.Next.Point);




				for (int i = 1; i < pathData.Points.Length - 1; i++)
				{
					Gizmos.DrawLine (pathData.Points [i].Point, pathData.Points [i + 1].Point);
					Gizmos.DrawSphere (pathData.Points [i].Point, 0.6f);
				}
					
				Gizmos.DrawLine (pathData.Points [pathData.Points.Length - 1].Point, pathData.StartNode.Point);
				Gizmos.DrawSphere (pathData.Points [pathData.Points.Length - 1].Point, 0.6f);

				Gizmos.color = Color.white;
			}
		}
	}
}