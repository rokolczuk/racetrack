﻿using System;
using UnityEngine;
using Game.Model;

namespace Game.View
{
	[RequireComponent(typeof(Rigidbody))]
	public class Car: MonoBehaviour, IObstacle
	{
		[SerializeField]
		private Material material;

		[SerializeField]
		private MeshRenderer[] carBodyMeshes;

		private Material materialInstance;
		private Rigidbody carRigidbody;

		[SerializeField]
		private float accelerationMultiplier = 1.0f;

		[SerializeField]
		private float brakeMultiplier = 1.0f;

		[Header("Wheels and mud guards")]
		[SerializeField]
		private GameObject frontLeftWheel;

		[SerializeField]
		private GameObject frontRightWheel;

		[SerializeField]
		private GameObject rearLeftWheel;

		[SerializeField]
		private GameObject rearRightWheel;

		[SerializeField]
		private GameObject frontLeftMudGuard;

		[SerializeField]
		private GameObject frontRightMudGuard;

		private WheelCollider frontLeftWheelCollider;
		private WheelCollider frontRightWheelCollider;
		private WheelCollider rearLeftWheelCollider;
		private WheelCollider rearRightWheelCollider;

		[SerializeField]
		private float minAvoidDistance = 25f;

		[SerializeField]
		private float maxTimeDisabledToRevive = 5f;

		[SerializeField]
		private float minAngleToConsiderDisabled = 5f;

		[SerializeField]
		private float minSpeedToConsiderDisabled = 1f;


		private float timeDisabled = 0f;

		[SerializeField]
		private Player player;
		public Player Player
		{
			get 
			{
				return player;
			}
			set 
			{
				player = value;
				materialInstance.color = player.GetColor();
			}
		}

		public Vector3 Speed 
		{
			get 
			{
				return carRigidbody.velocity;
			}
		}
		private void Awake()
		{
			materialInstance = new Material(material);

			for(int i =0; i < carBodyMeshes.Length; i++)
			{
				carBodyMeshes[i].material = materialInstance;
			}

			carRigidbody = GetComponent<Rigidbody>();

			frontLeftWheelCollider = frontLeftWheel.GetComponent<WheelCollider>();
			frontRightWheelCollider = frontRightWheel.GetComponent<WheelCollider>();
			rearLeftWheelCollider = rearLeftWheel.GetComponent<WheelCollider>();
			rearRightWheelCollider = rearRightWheel.GetComponent<WheelCollider>();
		}

		public void Accelerate(float speed)
		{
			frontLeftWheelCollider.motorTorque =
			frontRightWheelCollider.motorTorque = speed * accelerationMultiplier * player.Velocity;
		}

		public void Brake(float brakePower)
		{
			rearLeftWheelCollider.brakeTorque =
				rearRightWheelCollider.brakeTorque = brakePower * brakeMultiplier;
		}

		public void Turn(float angle)
		{
			frontLeftWheelCollider.steerAngle = frontRightWheelCollider.steerAngle = angle;
		}

		private void Update()
		{
			Transform frontLeftWheelTransform = frontLeftWheel.transform;
			Transform frontRightWheelTransform = frontRightWheel.transform;

			frontLeftWheelTransform.localEulerAngles = new Vector3(frontLeftWheelTransform.localEulerAngles.x, frontLeftWheelCollider.steerAngle - frontLeftWheelTransform.localEulerAngles.z, frontLeftWheelTransform.localEulerAngles.z);
			frontRightWheelTransform.localEulerAngles = new Vector3(frontRightWheelTransform.localEulerAngles.x, frontRightWheelCollider.steerAngle - frontRightWheelTransform.localEulerAngles.z, frontRightWheelTransform.localEulerAngles.z);

			Transform frontLeftMudGuardTransform = frontLeftMudGuard.transform;
			Transform frontRightMudGuardTransform = frontRightMudGuard.transform;

			frontLeftMudGuardTransform.localEulerAngles = new Vector3(frontLeftMudGuardTransform.localEulerAngles.x, frontLeftWheelCollider.steerAngle - frontLeftMudGuardTransform.localEulerAngles.z, frontLeftMudGuardTransform.localEulerAngles.z);
			frontRightMudGuardTransform.localEulerAngles = new Vector3(frontRightMudGuardTransform.localEulerAngles.x, frontRightWheelCollider.steerAngle - frontRightMudGuardTransform.localEulerAngles.z, frontRightMudGuardTransform.localEulerAngles.z);

			frontLeftWheel.transform.Rotate(frontLeftWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
			frontRightWheel.transform.Rotate(frontRightWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
			rearLeftWheel.transform.Rotate(rearLeftWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
			rearRightWheel.transform.Rotate(rearRightWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);

			if(Mathf.Abs(transform.rotation.z) > minAngleToConsiderDisabled || carRigidbody.velocity.magnitude < minSpeedToConsiderDisabled)
			{
				timeDisabled += Time.deltaTime;
			}
			else 
			{
				timeDisabled = 0f;
			}

			if(timeDisabled >= maxTimeDisabledToRevive)
			{
				timeDisabled = 0f;
				Revive();
			}


		}

		private void Revive ()
		{
			transform.position = new Vector3(transform.position.x, transform.position.y + 6f, transform.position.z) - transform.forward * 10f;
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0f);
		}

		#region IObstacle implementation

		public float MinAvoidDistance
		{
			get
			{
				return minAvoidDistance;
			}
		}

		#endregion
	}
}