﻿using System;
using UnityEngine;

namespace Game.View
{
	public interface IObstacle
	{
		float MinAvoidDistance { get; }
		Transform transform {get ; }
	}
}

