﻿using UnityEngine;
using System.Collections;
using Shared.DependencyInjection;
using Shared;

namespace Game.View
{
	public class CameraRotator: MonoBehaviour
	{
		private static int maxNumAttemps = 10;

		[SerializeField]
		private Camera[] cameras;

		[SerializeField]
		private float CameraChangeInterval = 6f;

		[SerializeField]
		private float FirstRotationDelay = 10f;

		//so we don't show cars that are on the roof
		[SerializeField]
		private float MinCarRotationZ = 45f;

		[SerializeField]
		private float DistanceToCarTreshold;

		[Inject(Context.Game)]
		private RaceModel raceModel;

		private Camera currentCamera;

		private Car targetCar;

		private void Awake()
		{
			this.Inject();

			for(int i = 0; i < cameras.Length; i++)
			{
				cameras[i].enabled = false;
			}

			StartCoroutine(RotateCameras());
		}

		private IEnumerator RotateCameras ()
		{
			currentCamera = cameras[0];
			currentCamera.enabled = true;
			while(raceModel.Cars == null)
			{
				yield return new WaitForEndOfFrame();
			}

			SelectTarget();
			yield return new WaitForSeconds(FirstRotationDelay);

			while(this.enabled)
			{
				currentCamera.enabled = false;
				currentCamera = SelectCamera();
				currentCamera.enabled = true;
				SelectTarget();
				yield return new WaitForSeconds(CameraChangeInterval);
			}
		}

		private void SelectTarget ()
		{
			Car closestCar = raceModel.Cars[0];
			float closestCarDistance = Vector3.Distance(closestCar.transform.position, currentCamera.transform.position);

			for(int i = 1; i < raceModel.Cars.Count; i++)
			{
				float distance = Vector3.Distance(currentCamera.transform.position, raceModel.Cars[i].transform.position);

				if(distance <= closestCarDistance && Mathf.Abs(raceModel.Cars[i].transform.rotation.z) <= MinCarRotationZ)
				{
					closestCarDistance = distance;
					closestCar = raceModel.Cars[i];

				}
			}

			targetCar = closestCar;
		}

		private void Update()
		{
			if(targetCar != null) 
			{
				currentCamera.transform.LookAt(targetCar.transform);
			}
		}


			
		private Camera SelectCamera ()
		{
			Camera candidate = null;
			int numAttempts = 0;

			while(numAttempts++ <= maxNumAttemps && candidate == null)
			{
				Camera randomCamera = cameras[Random.Range(0, cameras.Length)];

				if(randomCamera == currentCamera)
				{
					continue;
				}

				for(int i = 0; i < raceModel.Cars.Count; i++)
				{
					if(Vector3.Distance(randomCamera.transform.position, raceModel.Cars[i].transform.position) <= DistanceToCarTreshold)
					{
						candidate = randomCamera;
					}
				}
			}

			if(candidate == null)
			{
				candidate = currentCamera;
			}

			return candidate;
		}
	}
}

