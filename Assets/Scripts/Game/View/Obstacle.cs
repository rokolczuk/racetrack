﻿using UnityEngine;

namespace Game.View
{
	[RequireComponent(typeof(Collider))]
	public class Obstacle : MonoBehaviour, IObstacle
	{
		private Collider obstacleCollider;

		[SerializeField]
		private float colliderSizeToAvoidDistanceMultiplier = 3f;

		[Header("debug")]
		[SerializeField]
		private float minAvoidDistance;

		private void Awake()
		{
			obstacleCollider = GetComponent<Collider>();
			minAvoidDistance = Mathf.Max(obstacleCollider.bounds.size.x, obstacleCollider.bounds.size.z) / 2 * colliderSizeToAvoidDistanceMultiplier;
		}

		#region IObstacle implementation

		public float MinAvoidDistance
		{
			get
			{
				return minAvoidDistance;
			}
		}

		#endregion

		private void OnDrawGizmos()
		{
			Gizmos.DrawWireSphere(transform.position, minAvoidDistance);
		}
	}
}

