using UnityEngine;
using Shared.DependencyInjection;
using Game;
using Menu;
using Shared.Events;
using Shared;
using Menu.Event;
using Shared.Assets;
using Game.Evemt;
using Shell.Events;
using System.Collections;

namespace Shell
{
	public class Bootstrap: MonoBehaviour
	{
		private InjectProvider injectProvider;

		private GameModule gameModule = new GameModule ();
		private MenuModule menuModule = new MenuModule ();

		private ModuleManager moduleManager;

		private EventDispatcher eventDispatcher;

		[SerializeField]
		private GameObject loadingScreen;

		private void Awake ()
		{
			CreateMappings ();
			moduleManager = new ModuleManager();
			InitialiseEvents ();
			loadingScreen.SetActive(false);
			moduleManager.LoadModule (menuModule);
		}

		private void CreateMappings ()
		{
			InjectProvider.CreateProvider (Context.Shell);
			injectProvider = InjectProvider.GetProvider (Context.Shell);
			injectProvider.Map<EventDispatcher> ();
			injectProvider.Map<IAssetManager, AssetManager>();

			eventDispatcher = injectProvider.GetInstance<EventDispatcher> ();
		}

		private void InitialiseEvents ()
		{
			eventDispatcher.AddEventListener<LaunchGameEvent> (OnLaunchGame);
			eventDispatcher.AddEventListener<LaunchMenuEvent> (OnLaunchMenu);
			eventDispatcher.AddEventListener<RestartGameEvent>(OnRestartGame);
			eventDispatcher.AddEventListener<ShowLoadingScreenEvent>(OnShowLoadingScreen);
			eventDispatcher.AddEventListener<HideLoadingScreenEvent>(OnHideLoadingScreen);
		}
			
		private void OnLaunchGame (LaunchGameEvent e)
		{
			moduleManager.LoadModule (gameModule);
		}

		private void OnLaunchMenu (LaunchMenuEvent e)
		{
			moduleManager.LoadModule (menuModule);
		}

		private void OnRestartGame (RestartGameEvent e)
		{
			moduleManager.UnloadModule();
			moduleManager.LoadModule(gameModule);
		}

		private void OnShowLoadingScreen (ShowLoadingScreenEvent e)
		{
			loadingScreen.SetActive(true);
		}

		private void OnHideLoadingScreen (HideLoadingScreenEvent e)
		{
			StartCoroutine(HideLoadingScreen());
		}

		private IEnumerator HideLoadingScreen ()
		{
			//allow unity to load scene
			yield return new WaitForSeconds(0.1f);
			loadingScreen.SetActive(false);
		}

		private void OnDestroy ()
		{
			eventDispatcher.RemoveEventListener<LaunchGameEvent> (OnLaunchGame);
			eventDispatcher.RemoveEventListener<LaunchMenuEvent> (OnLaunchMenu);
			eventDispatcher.RemoveEventListener<RestartGameEvent>(OnRestartGame);
			eventDispatcher.RemoveEventListener<ShowLoadingScreenEvent>(OnShowLoadingScreen);
			eventDispatcher.RemoveEventListener<HideLoadingScreenEvent>(OnHideLoadingScreen);
		}
	}
}