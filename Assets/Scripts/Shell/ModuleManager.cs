using Shared;
using UnityEngine.SceneManagement;
using UnityEngine;
using Shared.Loading;
using Shared.DependencyInjection;
using Shared.Events;
using Shell.Events;

namespace Shell
{
	public class ModuleManager
	{

		[Inject(Context.Shell)]
		private EventDispatcher eventDispatcher;

		private IModule currentModule;

		private bool loading = false;

		public ModuleManager ()
		{
			this.Inject();
		}

		public void LoadModule (IModule module)
		{
			if(loading)
			{
				Debug.LogWarning ("Module is already being loaded");
				return;
			}

			if (currentModule != module)
			{
				UnloadModule();

				currentModule = module;
				currentModule.Initialise ();

				if(currentModule.LoadingQueue != null)
				{
					loading = true;
					eventDispatcher.Dispatch<ShowLoadingScreenEvent>(new ShowLoadingScreenEvent());

					currentModule.LoadingQueue.OnLoadCompleted += OnModuleLoadingCompleted;
					currentModule.LoadingQueue.OnLoadFailed += OnModuleLoadingFailed;

					currentModule.LoadingQueue.Load();
				}
				else 
				{
					SceneManager.LoadScene (currentModule.GetSceneName ());
				}
			} 
			else
			{
				Debug.LogWarning ("Module already loaded");
			}
		}

		public void UnloadModule()
		{
			if (currentModule != null)
			{
				currentModule.Dispose ();
			}

			currentModule = null;
		}

		private void OnModuleLoadingCompleted (ILoadTask task)
		{
			DisposeLoader();
			SceneManager.LoadScene (currentModule.GetSceneName ());
			eventDispatcher.Dispatch<HideLoadingScreenEvent>(new HideLoadingScreenEvent());
		}

		private  void OnModuleLoadingFailed (ILoadTask task)
		{
			DisposeLoader();
		}

		private void DisposeLoader ()
		{
			loading = false;

			currentModule.LoadingQueue.OnLoadCompleted -= OnModuleLoadingCompleted;
			currentModule.LoadingQueue.OnLoadFailed -= OnModuleLoadingFailed;

		}
	}
}