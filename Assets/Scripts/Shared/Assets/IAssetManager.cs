﻿using System;

namespace Shared.Assets
{
	public interface IAssetManager
	{
		void Add<T>(string identifier, T asset);
		void Remove (string identifier);

		bool HasAsset (string path);

		T Load<T> (string identifier);
	}
}

