﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Shared.Assets
{
	public class AssetManager: IAssetManager
	{
		private Dictionary<string, object> assets = new Dictionary<string, object>();

		public AssetManager ()
		{
		}

		public void Add<T>(string identifier, T asset)
		{
			assets.Add(identifier, asset);
		}

		public void Remove (string identifier)
		{
			assets.Remove(identifier);
		}

		public bool HasAsset (string path)
		{
			return assets.ContainsKey(path);
		}

		public T Load<T> (string identifier)
		{
			if(assets.ContainsKey(identifier) && assets[identifier] is T)
			{
				return (T)(assets[identifier]);
			}

			Debug.LogError("No asset of identifier " + identifier + " found");
			return default(T);
		}
	}
}

