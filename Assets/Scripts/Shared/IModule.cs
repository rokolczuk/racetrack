using Shared.Loading;

namespace Shared
{
	public interface IModule
	{
		LoadingQueue LoadingQueue { get; }
		void Initialise ();

		void Dispose ();

		string GetSceneName ();
	}
}