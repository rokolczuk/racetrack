﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Shared.Assets;
using Shared;
using Shared.DependencyInjection;

[RequireComponent(typeof(Image))]
public class DynamicImage : MonoBehaviour {

	private Image image;

	[Inject(Context.Shell)]
	private IAssetManager assetManager;

	private string currentPath;

	private void Awake()
	{
		this.Inject();
		image = GetComponent<Image>();
	}

	public void LoadImage(string path)
	{
		if(currentPath != path)
		{
			Texture2D texture =  assetManager.Load<Texture2D>(path);
			image.sprite =  Sprite.Create(texture, new Rect(Vector2.zero, new Vector2(texture.width, texture.height)), Vector2.zero);
			currentPath = path;
		}
	}
}
