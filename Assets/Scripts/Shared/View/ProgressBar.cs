﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

	[SerializeField]
	private Image fillImage;

	private float progress;

	public float Progress
	{
		get
		{
			return progress;
		}
		set
		{
			progress = value;
			fillImage.fillAmount = value;
		}
	}
}
