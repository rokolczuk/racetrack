﻿using UnityEngine;
using System.Collections;
using System;

namespace Shared.Loading
{
	public delegate void LoadCompleted(Texture2D texture);
	public delegate void LoadFailed();

	public class ImageLoaderComponent: MonoBehaviour
	{
		public LoadCompleted LoadedCallback;
		public LoadFailed LoadFailedCallback;


		private WWW www;
		private Texture2D texture;
		

		public void Load(string path)
		{
			StartCoroutine(LoadImage(path));
		}

		private IEnumerator LoadImage (string path)
		{
			try
			{
				www = new WWW(path);
				yield return www;
				texture = new Texture2D(www.texture.width, www.texture.height);
				www.LoadImageIntoTexture(texture);
				LoadedCallback(texture);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
				LoadFailedCallback();
			}
			finally
			{
				www.Dispose();
			}
		}
	}
}

