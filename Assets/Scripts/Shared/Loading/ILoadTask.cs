﻿using System;

namespace Shared.Loading
{
	public delegate void TaskLoadCompleted(ILoadTask task);
	public delegate void TaskLoadFailed(ILoadTask task);

	public interface ILoadTask
	{
		void Load();
		void Unload ();
		event TaskLoadCompleted OnLoadCompleted;    
		event TaskLoadFailed OnLoadFailed;    
	}
}

