﻿using System;
using System.IO;
using UnityEngine;
using Shared.DependencyInjection;

namespace Shared.Loading
{
	public class JsonLoadTask<T>: ILoadTask
	{
		private string path;
		private InjectProvider injectProvider;

		private T parsedObject;

		public JsonLoadTask (string path, InjectProvider injectProvider = null)
		{
			this.path = path;
			this.injectProvider = injectProvider;
		}

		public T Data 
		{
			get 
			{
				return parsedObject;
			}
		}

		#region ILoadTask implementation

		public event TaskLoadCompleted OnLoadCompleted;

		public event TaskLoadFailed OnLoadFailed;

		public void Load ()
		{
			if(!File.Exists(path))
			{
				OnLoadFailed(this);
				return;
			}

			FileStream file = null;
			StreamReader streamReader = null;

			try 
			{
				file = new FileStream (path, FileMode.Open, FileAccess.Read);
				streamReader = new StreamReader( file );
				string jsonString = streamReader.ReadToEnd();
				parsedObject = JsonUtility.FromJson<T>(jsonString);

				if(injectProvider != null)
				{
					injectProvider.MapInstance<T>(parsedObject);
				}

				OnLoadCompleted(this);
			}
			catch(Exception e)
			{
				Debug.LogException(e);
				OnLoadFailed(this);
			}
			finally
			{
				file.Close();
				streamReader.Close();
			}
		}

		//No need to unload anything here
		public void Unload ()
		{
		}

		#endregion
	}
}

