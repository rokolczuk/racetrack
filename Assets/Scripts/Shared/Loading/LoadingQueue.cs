﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shared.Loading
{
	public class LoadingQueue: ILoadTask
	{
		private List<ILoadTask> tasks = new List<ILoadTask>();

		private int currentTaskIndex;
		
		public LoadingQueue ()
		{
		}

		public void AddTask(ILoadTask task)
		{
			tasks.Add(task);
		}

		#region ILoadTask implementation

		public virtual void Load ()
		{
			currentTaskIndex = 0;

			if(tasks.Count > 0)
			{
				LoadNext();
			}
			else 
			{
				OnLoadCompleted(this);
			}
	
		}

		public void Unload ()
		{
			for(int i = 0; i < tasks.Count; i++)
			{
				tasks[i].Unload();
			}
		}

		public event TaskLoadCompleted OnLoadCompleted;

		public event TaskLoadFailed OnLoadFailed;

		#endregion

		private void LoadNext ()
		{
			ILoadTask task = tasks[currentTaskIndex];
			task.OnLoadCompleted += OnTaskLoadCompleted;
			task.OnLoadFailed += OnTaskLoadFailed;
			task.Load();
		}

		private void OnTaskLoadCompleted (ILoadTask task)
		{
			task.OnLoadCompleted -= OnTaskLoadCompleted;
			task.OnLoadFailed -= OnTaskLoadFailed;

			if(currentTaskIndex + 1 < tasks.Count)
			{
				currentTaskIndex++;
				LoadNext();
			}
			else 
			{
				OnLoadCompleted(this);
			}
		}

		private void OnTaskLoadFailed (ILoadTask task)
		{
			Debug.LogError("Failed to load the task: " + task);

			OnLoadFailed(this);

			task.OnLoadCompleted -= OnTaskLoadCompleted;
			task.OnLoadFailed -= OnTaskLoadFailed;
		}
	}
}

