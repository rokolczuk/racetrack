﻿using System;
using System.IO;
using UnityEngine;
using Shared.DependencyInjection;
using Shared.Assets;

namespace Shared.Loading
{
	public class LoadImageTask: ILoadTask
	{
		[Inject(Context.Shell)]
		private IAssetManager assetManager;

		private ImageLoaderComponent imageLoaderComponent;

		private string path;

		public LoadImageTask (string path)
		{
			this.path = path;
			this.Inject();
		}


			
		#region ILoadTask implementation

		public event TaskLoadCompleted OnLoadCompleted;

		public event TaskLoadFailed OnLoadFailed;


		public void Load ()
		{
			if(assetManager.HasAsset(this.path))
			{
				OnLoadCompleted(this);
				return;
			}

			GameObject loaderGameObject = new GameObject();
			imageLoaderComponent = loaderGameObject.AddComponent<ImageLoaderComponent>();

			imageLoaderComponent.LoadedCallback += OnImageLoaded;
			imageLoaderComponent.LoadFailedCallback += OnImageLoadFailed;

			imageLoaderComponent.Load(this.path);
		}

		public void Unload ()
		{
			assetManager.Remove(path);
		}

		#endregion

		private void OnImageLoaded (Texture2D texture)
		{
			DisposeLoadingComponent();
			assetManager.Add<Texture2D>(path, texture);
			OnLoadCompleted(this);
		}

		private void OnImageLoadFailed ()
		{
			DisposeLoadingComponent();
			OnLoadFailed(this);
		}

		private void DisposeLoadingComponent ()
		{
			imageLoaderComponent.LoadedCallback -= OnImageLoaded;
			imageLoaderComponent.LoadFailedCallback -= OnImageLoadFailed;
			GameObject.Destroy(imageLoaderComponent.gameObject);
		}
	}
}

