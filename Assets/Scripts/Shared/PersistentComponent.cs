using UnityEngine;

namespace Shared
{
	public class PersistentComponent : MonoBehaviour
	{
		private void Awake ()
		{
			DontDestroyOnLoad (gameObject);
		}
	}
}