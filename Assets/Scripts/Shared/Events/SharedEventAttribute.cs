using System;

namespace Shared.Events
{
	[AttributeUsage (AttributeTargets.Class)]
	public class SharedEventAttribute: Attribute
	{
		public readonly Context ShareToContext;

		public SharedEventAttribute (Context shareToContext)
		{
			ShareToContext = shareToContext;
		}
	}
}