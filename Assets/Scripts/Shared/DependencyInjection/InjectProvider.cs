using System.Collections.Generic;
using System;
using UnityEngine.Assertions;
using UnityEngine;

namespace Shared.DependencyInjection
{

	public class InjectProvider
	{
		private static Dictionary<Context, InjectProvider> providers = new Dictionary<Context, InjectProvider> ();

		private Dictionary<string, Type> typeMappings = new Dictionary<string, Type> ();
		private Dictionary<string, object> instanceMappings = new Dictionary<string, object> ();

		public static InjectProvider GetProvider (Context context)
		{
			Assert.IsTrue (providers.ContainsKey (context), "InjectProvider for context " + context + " doesn't exists");

			return providers [context];
		}

		public static void CreateProvider (Context context)
		{
			Assert.IsFalse (providers.ContainsKey (context), "InjectProvider for context " + context + " already exists");

			providers [context] = new InjectProvider ();
		}

		public static void DestroyProvider (Context context)
		{
			Assert.IsTrue (providers.ContainsKey (context), "InjectProvider for context " + context + " doesn't exists");

			providers [context].Destroy ();
			providers.Remove (context);
		}

		public static bool HasProvider (Context context)
		{
			return providers.ContainsKey (context);
		}

		public void MapInstance (Type type, object instance)
		{
			Assert.IsNotNull (instance, "Tried to map null instance");
			Assert.IsTrue (type.IsInstanceOfType (instance), "Mapped instance is incompatible");

			string mappingId = type.FullName;
	
			Assert.IsFalse (instanceMappings.ContainsKey (mappingId), "Mapping for type " + mappingId + " already exists");

			instanceMappings.Add (mappingId, instance);
		}

		public void MapInstance<T> (object instance)
		{
			MapInstance (typeof(T), instance);
		}

		public void Map (Type mappedType, Type toType)
		{
			string mappingId = mappedType.FullName;
			Assert.IsFalse (typeMappings.ContainsKey (mappingId), "Mapping for type " + mappingId + " already exists");

			typeMappings.Add (mappingId, toType);
		}

		public void Map<T1, T2> ()
		{
			Type mappedType = typeof(T1);
			Type toType = typeof(T2);

			string mappingId = mappedType.FullName;
			Assert.IsFalse (typeMappings.ContainsKey (mappingId), "Mapping for type " + mappingId + " already exists");

			typeMappings.Add (mappingId, toType);
		}

		public void Map<T> ()
		{
			Type mappedType = typeof(T);
			Type toType = typeof(T);

			string mappingId = mappedType.FullName;
			Assert.IsFalse (typeMappings.ContainsKey (mappingId), "Mapping for type " + mappingId + " already exists");

			typeMappings.Add (mappingId, toType);
		}

		public void Unmap (Type type)
		{
			string mappingId = type.FullName;
			Assert.IsTrue (typeMappings.ContainsKey (mappingId) || instanceMappings.ContainsKey (mappingId), "Mapping for type " + mappingId + " doesn't exists");

			if (typeMappings.ContainsKey (mappingId))
			{
				typeMappings.Remove (mappingId);
			}

			if (instanceMappings.ContainsKey (mappingId))
			{
				instanceMappings.Remove (mappingId);
			}
		}

		public void Unmap<T> ()
		{
			Unmap (typeof(T));
		}

		public object GetInstance (Type type)
		{
			string mappingId = type.FullName;
			Assert.IsTrue (typeMappings.ContainsKey (mappingId) || instanceMappings.ContainsKey (mappingId), "Mapping for type " + mappingId + " doesn't exists");

			if (instanceMappings.ContainsKey (mappingId))
			{
				return instanceMappings [mappingId];

			} else if (typeMappings.ContainsKey (mappingId))
			{
				Type mappedType = typeMappings [mappingId];
		
				object instance = Activator.CreateInstance (mappedType);
				instance.Inject ();

				instanceMappings [mappingId] = instance;
				return instance;
			}
	
			Debug.LogError ("Oh lord how did I get here");
			return null;
		}

		public T GetInstance<T> ()
		{
			return (T)GetInstance (typeof(T));
		}

		public void Destroy ()
		{
			typeMappings.Clear ();
			instanceMappings.Clear ();
		}

		public bool HasMapping (Type type)
		{
			string mappingId = type.FullName;
			return typeMappings.ContainsKey (mappingId) || instanceMappings.ContainsKey (mappingId);
		}

		public bool HasMapping<T> ()
		{
			return HasMapping (typeof(T));
		}
	}
}